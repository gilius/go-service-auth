package models

import (
	"github.com/golang-jwt/jwt"
	"time"
)

type ServiceClaims struct {
	Id         string `json:"id"`
	Service    bool   `json:"service"`
	ExpiresAt  int64  `json:"exp,omitempty"`
	jwt.Claims `json:"-"`
}

func (c ServiceClaims) IsExpired() bool {
	return time.Now().After(time.Unix(c.ExpiresAt, 0))
}
