package models

type TokenResponse struct {
	JWT          string `json:"jwt"`
	RefreshToken string `json:"refreshToken"`
}
