package models

type ServiceCredentials struct {
	Id     string `json:"id"`
	Secret string `json:"secret"`
}
