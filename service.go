package service_auth

import (
	"bitbucket.org/gilius/go-service-auth/models"
	"bitbucket.org/gilius/go-toolbox"
	"bytes"
	"encoding/json"
	"errors"
	"github.com/golang-jwt/jwt"
	"io"
	"log"
	"net/http"
	"strings"
	"sync"
)

type Service interface {
	SignRequest(request *http.Request) error
}

// service manages service session
type service struct {
	authDomain  string
	token       models.TokenResponse
	claims      *models.ServiceClaims
	logger      *log.Logger
	lock        *sync.Mutex
	credentials models.ServiceCredentials
}

const networkRetryCount = 3
const messageExpiredRefreshToken = "expired_refresh_token"

const endpointAuth = "/auth/services/login"
const endpointRefresh = "/auth/refresh"

// SignRequest signs a http request with auth
func (s *service) SignRequest(request *http.Request) error {
	token, err := s.getToken()
	if err != nil {
		return err
	}
	request.Header.Add("Authorization", "Bearer "+token.JWT)
	return nil
}

func (s *service) getClaims() (*models.ServiceClaims, error) {
	if s.claims == nil {
		body := strings.Split(s.token.JWT, ".")[1]

		decoded, err := jwt.DecodeSegment(body)
		if err != nil {
			s.logger.Println("Failed to parse JWT:")
			s.logger.Println(err)
			return nil, ErrInternal
		}

		var claims *models.ServiceClaims
		err = json.Unmarshal(decoded, &claims)
		if err != nil {
			s.logger.Println("Failed to unmarshal JWT body:")
			s.logger.Println(err)
			return nil, ErrInternal
		}
		s.claims = claims

	}
	return s.claims, nil
}

func (s *service) getToken() (*models.TokenResponse, error) {
	//synchronized
	s.lock.Lock()
	defer s.lock.Unlock()

	//get current claims data
	claims, err := s.getClaims()
	if err != nil {
		return nil, err
	}

	//if claims expired
	if claims.IsExpired() {
		//try to refresh
		errRefresh := handleRetry(networkRetryCount, s.refresh)

		//If refresh token expired
		if errors.Is(ErrExpired, errRefresh) {
			//try to re-login
			errAuth := handleRetry(networkRetryCount, s.authenticate)

			//If re-login failed
			if errAuth != nil {
				return nil, errAuth
			}
		}

		//if other refresh error - throw
		if errRefresh != nil {
			return nil, errRefresh
		}
	}

	//Return token
	return &s.token, nil
}

func (s *service) refresh() error {
	s.claims = nil

	data, err := json.Marshal(s.token.RefreshToken)
	if err != nil {
		s.logger.Println("Failed to json encode credentials")
		return ErrInternal
	}
	bodyBuffer := bytes.NewBuffer(data)

	response, err := http.Post(s.authDomain+endpointRefresh, "application/json", bodyBuffer)
	if err != nil {
		s.logger.Println("Failed auth request")
		s.logger.Println(err)
		return ErrNetwork
	}

	if response.StatusCode != http.StatusOK {
		//Getting body
		body, errBody := io.ReadAll(response.Body)
		if errBody != nil {
			s.logger.Println("Failed to read response body")
			s.logger.Println(errBody)
			return ErrInternal
		}

		//Parsing body
		var responseObj toolbox.ResponseObject
		errDecode := json.Unmarshal(body, &responseObj)
		if errDecode != nil {
			s.logger.Println("Failed to decode response body")
			s.logger.Println(string(body))
			s.logger.Println(errDecode)
			return ErrInternal
		}

		//Handle expired
		if responseObj.Message == messageExpiredRefreshToken {
			return ErrExpired
		}

		//Other error as internal
		s.logger.Printf("Invalid status code on refresh: %d. %s\n", response.StatusCode, string(body))
		return ErrInternal
	}

	err = toolbox.JsonDecode(response, &s.token)
	if err != nil {
		s.logger.Println("Failed auth request decode")
		s.logger.Println(err)
		return ErrInternal
	}

	return nil
}

func (s *service) authenticate() error {

	data, err := json.Marshal(s.credentials)
	if err != nil {
		s.logger.Println("Failed to json encode credentials")
		s.logger.Println(err)
		return ErrInternal
	}
	bodyBuffer := bytes.NewBuffer(data)

	response, err := http.Post(s.authDomain+endpointAuth, "application/json", bodyBuffer)
	if err != nil {
		s.logger.Println("Failed login request")
		s.logger.Println(err)
		return ErrNetwork
	}

	if response.StatusCode != http.StatusOK {
		if response.StatusCode == http.StatusBadRequest {
			s.logger.Printf("Invalid login credentials: %v\n", s.credentials)
			return ErrInvalidCredentials
		}

		//Getting body
		body, errBody := io.ReadAll(response.Body)
		if errBody != nil {
			s.logger.Println("Failed to read response body")
			s.logger.Println(errBody)
			return ErrInternal
		}

		//Other error as internal
		s.logger.Printf("Invalid status code on login: %d. %s\n", response.StatusCode, string(body))
		return ErrInternal
	}

	err = toolbox.JsonDecode(response, &s.token)
	if err != nil {
		s.logger.Println("Failed to decode login response")
		s.logger.Println(err)
		return ErrInternal
	}

	return nil
}

// NewServiceAuth Creates a new service
func NewServiceAuth(appId string, appSecret string, authDomain string, logger *log.Logger) (Service, error) {
	instance := &service{
		authDomain: authDomain,
		lock:       &sync.Mutex{},
		logger:     logger,
		credentials: models.ServiceCredentials{
			Id:     appId,
			Secret: appSecret,
		},
	}

	err := handleRetry(networkRetryCount, instance.authenticate)
	if err != nil {
		return nil, err
	}

	return instance, nil
}
