package service_auth

import "errors"

func handleRetry(times int, handler func() error) error {
	tries := 0
	for {
		tries++

		err := handler()
		if err == nil {
			return nil
		}

		if errors.Is(ErrNetwork, err) && tries <= times { //if network error and re-try still allowed
			continue //go again
		}

		return err //return error o
	}
}
