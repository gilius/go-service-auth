package service_auth

import "errors"

var (
	ErrInvalidCredentials = errors.New("invalid_credentials")
	ErrNetwork            = errors.New("network")
	ErrInternal           = errors.New("internal")
	ErrExpired            = errors.New("expired")
)
