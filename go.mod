module bitbucket.org/gilius/go-service-auth

go 1.20

require (
	bitbucket.org/gilius/go-toolbox v1.1.21
	github.com/golang-jwt/jwt v3.2.2+incompatible
)

require (
	github.com/gorilla/websocket v1.5.1 // indirect
	go.mongodb.org/mongo-driver v1.13.0 // indirect
	golang.org/x/net v0.18.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
)
